        <script type="text/javascript">
            function run() {
                document.getElementById('add_new_element').addEventListener('click', function(event){
                    event.preventDefault();
                    var item = document.getElementById('element-stack-item-template').innerHTML;
                    var count = document.getElementById('element-stack').childElementCount;
                    var index;
                    if (count === 0) {
                        index = parseInt(count)+1;
                    } else {
                        var lastIndex = document.getElementById('element-stack').lastElementChild.attributes.index.value;
                        index = parseInt(lastIndex)+1;
                    }
                    item = item.replace('__INDEX__',index);
                    item = item.replace('__INDEX__',index);
                    document.getElementById('element-stack').innerHTML += item;
                    var deleteItems = document.querySelectorAll('#element-stack .item');
                    for (var i=0; i<deleteItems.length; i++) {
                        document.querySelector('#element-stack .item[index="'+i+'"]').addEventListener('click', function(event){
                            event.preventDefault();
                            this.remove();
                        });
                    }
                });
            }
            if (document.readyState === 'loading') {
                document.addEventListener('DOMContentLoaded', run);
            } else { run(); }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Simple dynamic form</h1>

            <form action="" method="post">
                <input type="hidden" name="sample_dynamic_form">

                <div id="element-stack">
                </div>

                <br>
                <button id="add_new_element">Add</button>
                <hr>
                <input type="submit" value="Submit">
            </form>
        </div>

        <div style="display: none" id="element-stack-item-template">
            <div class="item" index="__INDEX__">
                <input type="text" name="element___INDEX__">
                <button class="remove-item">Remove</button>
            </div>
        </div>
    </body>